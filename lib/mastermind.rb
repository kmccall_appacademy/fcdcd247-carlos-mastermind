class Code
  attr_reader :pegs
  PEGS = {
    red: 'r',
    green: 'g',
    blue: 'b',
    yellow: 'y',
    orange: 'o',
    purple: 'p'
  }

  def initialize(pegs)
    if pegs.nil?
      raise ArgumentError
    end
    @pegs = pegs
  end

  def self.parse(string)
    split = string.downcase.chars
    if split.uniq.all? { |x| PEGS.values.include?(x) }
      return Code.new(split)
    else
      raise ArgumentError
    end
  end

  def self.random
    Code.new([PEGS.values.sample, PEGS.values.sample, PEGS.values.sample, PEGS.values.sample])
  end

  def exact_matches(other_code)
    matches = 0
    4.times do |i|
      matches += 1 if self[i] == other_code[i]
    end
    matches
  end

  def near_matches(other_code)
    hash1 = {}
    hash2 = {}
    4.times do |i|
      unless self[i] == other_code[i]
        if hash1.keys.include?(self[i])
          hash1[self[i]] += 1
        else
          hash1[self[i]] = 1
        end
        if hash2.keys.include?(other_code[i])
          hash2[other_code[i]] += 1
        else
          hash2[other_code[i]] = 1
        end
      end
    end
    nearmatches = 0
    (hash1.keys.uniq & hash2.keys.uniq).each do |color|
      nearmatches += [hash1[color], hash2[color]].min
    end
    nearmatches
  end

  def ==(other_code)
    if other_code.is_a?(Code)
      return self.pegs == other_code.pegs
    end
    false
  end

  def [](key)
    @pegs[key]
  end

end

class Game
  attr_accessor :secret_code
  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(other_code)
    near = @secret_code.near_matches(other_code)
    exact = @secret_code.exact_matches(other_code)
    puts("#{near} near matches")
    puts("#{exact} exact matches")
  end
end
